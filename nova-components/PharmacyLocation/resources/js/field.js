import IndexField from './components/IndexField'
import DetailField from './components/DetailField'
import FormField from './components/FormField'

Nova.booting((app, store) => {
    app.component('index-pharmacy-location', IndexField)
    app.component('detail-pharmacy-location', DetailField)
    app.component('form-pharmacy-location', FormField)
})

<?php

namespace Hong195\PharmacyLocation;

use Laravel\Nova\Fields\Field;

class PharmacyLocation extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'pharmacy-location';
}

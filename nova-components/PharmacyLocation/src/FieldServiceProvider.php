<?php

namespace Hong195\PharmacyLocation;

use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Events\ServingNova;
use Laravel\Nova\Nova;

class FieldServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::serving(function (ServingNova $event) {
            Nova::script('pharmacy-location', __DIR__.'/../dist/js/field.js');
            Nova::style('pharmacy-location', __DIR__.'/../dist/css/field.css');
            Nova::script('pharmacy-yandex-map', 'https://api-maps.yandex.ru/2.1/?apikey=b056839c-74c0-4142-9543-ffb432c418fd&lang=ru_RU');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

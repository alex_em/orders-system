<?php
//  ██████╗██╗     ██╗ ██████╗██╗   ██╗    ██╗    ██╗██████████╗
// ██╔════╝██║     ██║██╔════╝██║ ██╔═╝    ██║    ██║      ██╔═╝
// ██║     ██║     ██║██║     ████╔═╝      ██║    ██║    ██╔═╝
// ██║     ██║     ██║██║     ██║ ██╗      ██║    ██║  ██══╝
// ╚██████╗███████╗██║╚██████╗██║   ██╗ ██╗█████████║██████████╗
//  ╚═════╝╚══════╝╚═╝ ╚═════╝╚═╝   ╚═╝ ╚═╝╚════════╝╚═════════╝
namespace click;
/**
 * @name PaymentsStatus class, the some std payment status
 *
 * @example
 *      if($payment['status'] == PaymentsStatus::CONFIRMED){
 *          ...
 *      }
 */
class PaymentsStatus
{
    /** @var INPUT string */
    const NEW = 'new';
    const WAITING = 'waiting';
    /** @var PREAUTH string */
    const PREAUTH = 'preauth';
    /** @var CONFIRMED string */
    const CONFIRMED = 'confirmed';
    /** @var REJECTED string */
    const REJECTED = 'rejected';
    /** @var REFUNDED string */
    const REFUNDED = 'refunded';
    /** @var ERROR string */
    const ERROR = 'error';

const STATUS_LABEL = [
        PaymentsStatus::NEW       => "Не оплачено",
        PaymentsStatus::WAITING   => "Ожидания оплаты",
        PaymentsStatus::CONFIRMED => "Оплачено",
        PaymentsStatus::REJECTED  => "Оплата отменено",
    ];
}

{{config('app.url')}}{{getCurPageParams()}}
<b>{{ $appName }}</b> ({{ $level_name }})
Env: {{ $appEnv }}
[{{ $datetime->format('Y-m-d H:i:s') }}] {{ $appEnv }}.{{ $level_name }} {{
    \Illuminate\Support\Str::limit($formatted, 1000, "....") }}

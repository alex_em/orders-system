<?php

use App\Http\Controllers\OnlinePharmacyOrderApiController;
use App\Services\Payment\ClickInvoiceService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Orion\Facades\Orion;
use Payment\Click;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([], function() {
    Route::post('products/search', [App\Http\Controllers\Api\ApiGoodsController::class, 'index']);
    Route::post('products/search/{id}', [App\Http\Controllers\Api\ApiGoodsController::class, 'show']);
})
    ->middleware('auth:sanctum');

Route::post('order', function () {
    return response()->json();
});

Route::put('order/{orderId}', function () {
    return response()->json();
});
Route::any('click/{action}', function (Request $request, $action) {
    $click=new ClickInvoiceService();
    $click->log("CheckReq:" . $click->helper->url);
    try {
        $click->checkRequests($action);
    } catch (Throwable $e) {
        $click->log("Error: ".print_r($e->getMessage(), 1));
        $click->response($e->error??[
            'error'      => -3,
            'error_note' => 'Something is wrong:'.$e->getMessage()
        ]);
    }
    return "ok";
});

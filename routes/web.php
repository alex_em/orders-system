<?php

use App\Models\OnlinePharmacy\Entity\Order;
use App\Models\Payments;
use App\Services\Delphi\ClickOrderService;
use App\Services\OnlinePharmacy\NotifyPharmacy;
use App\Services\OnlinePharmacy\OrderService;
use App\Services\OnlinePharmacy\StatusService;
use App\Services\PayMe\SubscribeAPI;
use App\Services\Payment\ConvertSumToTins;
use App\Services\Payment\PharmacyPayments;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Models\Delphi\Order as DelphiOrder;
use App\Models\OnlinePharmacy\Entity\Order as FOrder;
use App\Services\Delphi\PayMeOrderService;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/ztest/', function () {
    #$delphiOrderService = app()->make(ClickOrderService::class);
    #$order = Order::find(134);
    #$delphiOrderService->create($order, "14566027");


    #$delphiOrderService = app()->make(PayMeOrderService::class);
       # $order = FOrder::find(127);
        #$receiptId="655da1ed89c0cc0d574c3b3a";
        #$delphiOrderService->create($order, $receiptId);

});
Route::get('/test', function () {
    $order = \App\Models\OnlinePharmacy\Entity\Order::findOrFail(76);
    dd($order->items);
});

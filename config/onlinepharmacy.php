<?php

return [
    'api_url' => env('ONLINE_PHARMACY_API_URL', 'http://192.168.100.11/api/v1/'),
    'token' => env('ONLINE_PHARMACY_API_KEY', '')
];

<?php

namespace App\Jobs;

use App\Exceptions\PayMeException;
use App\Models\OnlinePharmacy\Entity\Order;
use App\Models\PayMe;
use App\Services\Delphi\PayMeOrderService;
use App\Services\PayMe\CheckStates;
use App\Services\PayMe\SubscribeAPI;
use App\Services\Payment\PharmacyPayments;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Client\RequestException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckPayMeReceiptStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public int $tries = 21;

    public int $timeout = 30;
    private PayMe $payMe;
    private Order $order;
    private readonly string $receiptId;

    public function __construct(string $receiptId, Order $order)
    {
        $this->receiptId = $receiptId;
        $this->payMe = PharmacyPayments::getByPharmacyId($order->store_id)->getPaymeCredentials();
        $this->order = $order;
    }

    /**
     * @throws RequestException
     * @throws PayMeException
     * @throws BindingResolutionException
     */
    public function handle(): void
    {
        $subscribeAPI = new SubscribeAPI($this->payMe->getCashId(), $this->payMe->getCashKey());

        $receipt = $subscribeAPI->checkReceipt($this->receiptId);

        if (CheckStates::isPaid((int) $receipt['result']['receipt']['state'])) {
            $this->onSuccess();
            return;
        }

        if ($this->attempts() < 21) {
            $this->release(30);
            return;
        }

        $this->order->fresh()->setStatus(Order::CANCELED);
        $subscribeAPI->cancelReceipt($this->receiptId);
        $this->fail();
    }

    public function backoff(): int
    {
        return 30;
    }

    /**
     * @throws BindingResolutionException
     */
    public function onSuccess(): void
    {
        /** @var PayMeOrderService $delphiOrderService */
        $delphiOrderService = app()->make(PayMeOrderService::class);
        $order = $this->order->fresh();
        $order->paid();
        $delphiOrderService->create($order, $this->receiptId);

        $this->delete();
    }
}

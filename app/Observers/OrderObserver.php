<?php

namespace App\Observers;

use App\Models\OnlinePharmacy\Entity\Order;
use App\Models\OnlinePharmacy\Logger;
use Throwable;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     */
    public function created(Order $order): void
    {
        Logger::addOrderLog($order->id, 'Order created');
    }

    /**
     * Handle the Order "updated" event.
     */
    public function updated(Order $order): void
    {
        $changes = $order->getDirty();
        if (array_key_exists('updated_at', $changes)) {
            unset($changes['updated_at']);
        }
        if ($changes) {
            ///send notif
            if (isset($changes["status_id"])) {
                try {
                    file_get_contents('http://192.168.100.11/notif/' . $order->id);
                } catch (Throwable $e) {

                }
            }
            $changes = Logger::array2string($changes);


            Logger::addOrderLog($order->id, 'Order updated: ' . $changes);
        }

    }

    /**
     * Handle the Order "deleted" event.
     */
    public function deleted(Order $order): void
    {
        Logger::addOrderLog($order->id, 'Order deleted!');
    }

    /**
     * Handle the Order "restored" event.
     */
    public function restored(Order $order): void
    {
        Logger::addOrderLog($order->id, 'Order restored!');
    }

    /**
     * Handle the Order "force deleted" event.
     */
    public function forceDeleted(Order $order): void
    {
        Logger::addOrderLog($order->id, 'Order forceDeleted!');
    }
}

<?php

namespace App\Services\PayMe;

use App\Exceptions\PayMeException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Client\RequestException;

class SubscribeAPI
{
    const RECEIPT_TYPE = 0;

    private API $api;

    /**
     * @throws BindingResolutionException
     */
    public function __construct(private readonly string $cashierId, private readonly string $cashierPassword)
    {
        $this->api = new API($this->cashierId, $this->cashierPassword);
    }

    /**
     * @throws PayMeException
     * @throws RequestException
     */
    public function createReceipt(int $amount, array $orderData, array $details)
    {
        return $this->api->makeRequest(SubscribeAPIMethods::CREATE_RECEIPT, [
            "amount" => $amount,
            "account" => $orderData,
            "detail" => [
                "receipt_type" => self::RECEIPT_TYPE,
                ...$details
            ],
        ]);
    }

    /**
     * @throws RequestException
     * @throws PayMeException
     */
    public function checkReceipt(string $checkId)
    {
        return $this->api->makeRequest(SubscribeAPIMethods::GET_RECEIPT, [
            "id" => $checkId
        ]);
    }

    /**
     * @throws RequestException
     * @throws PayMeException
     */
    public function sendReceipt(string $checkId, string $phone)
    {
        return $this->api->makeRequest(SubscribeAPIMethods::SEND_RECEIPT, [
            "id" => $checkId,
            "phone" => $phone
        ]);
    }

    /**
     * @throws RequestException
     * @throws PayMeException
     */
    public function cancelReceipt(string $checkId)
    {
        return $this->api->makeRequest(SubscribeAPIMethods::CANCEL_RECEIPT, [
            "id" => $checkId
        ]);
    }
}

<?php

namespace App\Services\PayMe;

use App\Exceptions\PayMeException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Str;
use Illuminate\Http\Client\PendingRequest;

class API
{
    const JSON_RPC_VERSION = '2.0';
    private PendingRequest $client;

    /**
     * @throws BindingResolutionException
     */
    public function __construct(private readonly string $cashierId, private readonly string $cashierPassword)
    {
        $this->client = app()->make(PendingRequest::class);
        $this->client->baseUrl(config('payme.host_url'));

        $this->client->withHeaders([
            'X-Auth' => $this->cashierId . ':' . $this->cashierPassword
        ]);
    }

    /**
     * @throws RequestException
     * @throws PaymeException
     */
    public function makeRequest(SubscribeAPIMethods $method, array $params = [])
    {
        $response = $this->client->post('', [
                'jsonrpc' => self::JSON_RPC_VERSION,
                'id' => $this->generateId(),
                'method' => $method->value,
                'params' => $params,
            ]
        )
            ->throw(fn($r, $e) => self::handleFailedRequest($e));

        $json = $response->json();

        if (isset($json['error'])) {
            throw new PaymeException($json['error']['message'], $json['error']['code']);
        }

        return $json;
    }

    private function generateId(): string
    {
        return Str::ulid()->toBase58();
    }

    /**
     * @throws PayMeException
     */
    private function handleFailedRequest(\Exception $e)
    {
        throw new PaymeException($e->getMessage(), $e->getCode());
    }
}

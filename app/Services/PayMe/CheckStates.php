<?php

namespace App\Services\PayMe;

enum CheckStates : int
{
    case CREATED = 0;

    case PAID = 4;

    case CANCELED = 50;

    public static function isPaid(int $state): bool
    {
        return $state === self::PAID->value;
    }
}

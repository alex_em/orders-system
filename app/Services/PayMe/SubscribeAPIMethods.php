<?php

namespace App\Services\PayMe;

enum SubscribeAPIMethods : string
{
    case CREATE_RECEIPT = 'receipts.create';
    case CHECK_RECEIPT = 'receipts.check';
    case SEND_RECEIPT = 'receipts.send';
    case CANCEL_RECEIPT = 'receipts.cancel';
    case GET_RECEIPT = 'receipts.get';
}

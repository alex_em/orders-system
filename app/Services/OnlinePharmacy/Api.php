<?php

namespace App\Services\OnlinePharmacy;

use App\Exceptions\OnlinePharmacyException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Api
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => config('onlinepharmacy.api_url'),
                'headers' => [
                    'x-api-key' => config('onlinepharmacy.token')
                ]
            ]
        );
    }

    /**
     * @throws GuzzleException
     * @throws OnlinePharmacyException
     */
    public function edit(int $orderId, array $data)
    {
        $request = $this->client->patch("order/edit/$orderId", [
            'form_params' => $data
        ]);

        if ($request->getStatusCode() !== 200) {
            throw new OnlinePharmacyException('Ошибка при редактировании заказа');
        }
    }

    /**
     * @throws GuzzleException
     * @throws OnlinePharmacyException
     */
    public function getStatuses(): array
    {
        $request = $this->client->get("status-list");

        if ($request->getStatusCode() !== 200) {
            throw new OnlinePharmacyException('Ошибка получения статусов заказа');
        }

        return json_decode($request->getBody()->getContents());
    }
}

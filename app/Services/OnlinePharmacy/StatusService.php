<?php

namespace App\Services\OnlinePharmacy;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;

class StatusService
{
    public function __construct(private readonly Api $api)
    {
    }

    /**
     * @throws GuzzleException
     */
    public function getAll() : ?array
    {
        return Cache::remember('e-pharmacy-statuses', 3600, function() {
            $statuses = $this->api->getStatuses();
            $mappedStatuses = [];

            foreach ($statuses as $status) {
                $mappedStatuses[$status?->id] = $status?->name?->ru;
            }

            return $mappedStatuses;
        });
    }
}

<?php

namespace App\Services\OnlinePharmacy;

use App\Exceptions\OnlinePharmacyException;
use App\Models\Order;
use GuzzleHttp\Exception\GuzzleException;

class NotifyPharmacy
{
    public function __construct(private readonly Api $api)
    {
    }

    /**
     * @throws OnlinePharmacyException
     * @throws GuzzleException
     */
    public function notify(int $pharmacyOrderId, Order $order): void
    {
        $this->api->edit($pharmacyOrderId, $this->mapData($order));
    }

    private function mapData(Order $order): array
    {
        return [
            'delphi_id' => $order->id,
            'sum' => $order->getTotal(),
            'note' => $order->description,
            'store_id' => $order->pharmacy_id,
            'status_id' => $order->status,
            'products' => $this->mapGoods($order->getItems()),
        ];
    }

    private function mapGoods(iterable $goods): array
    {
        $mappedGoods = [];

        foreach ($goods as $good) {
            $mappedGoods[] = [
                'delphi_id' => $good['good']['value'],
                'price' => $good['price'],
                'count' => $good['quantity'],
            ];
        }

        return $mappedGoods;
    }
}

<?php

namespace App\Services\OnlinePharmacy;

use App\Models\Client;
use App\Models\Delphi\Good;
use App\Models\Order;

class OrderService
{
    public function create(array $orderData): Order
    {
        $client = Client::firstOrCreate(
            ['phone' => $orderData['user']['phone']],
            [
                'name' => $orderData['fio'],
                'online_pharmacy_id' => $orderData['store'],
                'address' => $orderData['user']['address'] ?? "",
            ],
        );

        $order = new Order([
            'client_id' => $client->id,
            'pharmacy_id' => $orderData['store'] ?? null,
            'description' => $orderData['note'] ?? null,
            'status' => Order::NEW,
        ]);

        $order->saveQuietly();

        $this->update($order->id, $orderData);

        return $order;
    }

    public function update(int $id, array $orderData)
    {
        $order = Order::findOrFail($id);

        $mainFieldsToUpdate = array_filter(
            [
                'pharmacy_id' => $orderData['store'] ?? null,
                'description' => $orderData['note'] ?? null,
                'status' => $orderData['status'] ?? null,
            ],
            fn($value) => $value !== null);

        if (!empty($mainFieldsToUpdate)) {
            $order->updateQuietly($mainFieldsToUpdate);
        }

        $secondaryFieldsToUpdate = [
            'pharmacy_order_id' => $orderData['id'] ?? null,
            'delivery' => $orderData['delivery'] ?? null,
            'payment' => $orderData['payment'] ?? null,
            'promo' => $orderData['promo'] ?? null,
            'is_self' => $orderData['is_self'] ?? false,
            'phone' => $orderData['phone'] ?? null,
            'address' => $orderData['address'] ?? null,
            'location' => $orderData['location']
        ];

        if (!empty($orderData['products'])) {
            $order->items = $this->getGoods($orderData['products']);
        }

        $data = $order->data ?? [];
        $data = array_merge($data, array_filter($secondaryFieldsToUpdate, fn($value) => $value !== null));
        $order->data = $data;

        $order->saveQuietly();

        return $order;
    }

    private function getGoods(array $goods): array
    {
        $mappedGoods = [];

        foreach ($goods as $good) {
            $delphiGood = Good::where('GoodId', $good['delphi_id'])->firstOrFail();
            $mappedGoods[] = [
                'good' => [
                    'label' => $delphiGood->Name,
                    'value' => $delphiGood->GoodId,
                ],
                'price' => $good['price'],
                'quantity' => $good['count'],
            ];
        }

        return $mappedGoods;
    }
}

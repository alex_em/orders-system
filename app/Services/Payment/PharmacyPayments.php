<?php

namespace App\Services\Payment;

use App\Models\Payments;

class PharmacyPayments
{
    public static function getByPharmacyId(int $pharmacyId)
    {
        return Payments::where('pharmacy_id', $pharmacyId)->first();
    }
}

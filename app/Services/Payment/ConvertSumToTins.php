<?php

namespace App\Services\Payment;

class ConvertSumToTins
{
    public function convert(int $sum): float
    {
        return $sum * 100;
    }
}

<?php

namespace App\Services\Payment;

use App\Exceptions\PayMeException;
use App\Jobs\CheckPayMeReceiptStatus;
use App\Models\OnlinePharmacy\Entity\Order;
use App\Models\PayMe;
use App\Models\PayMeReceipt;
use App\Services\PayMe\SubscribeAPI;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Client\RequestException;

class PayMeInvoiceService
{
    private readonly SubscribeAPI $subscribeAPI;

    private readonly PayMe $payMe;
    private ConvertSumToTins $sumToTins;

    /**
     * @throws BindingResolutionException
     */
    public function __construct(private readonly Order $order)
    {
        $this->payMe = PharmacyPayments::getByPharmacyId($order->store_id)->getPaymeCredentials();
        $this->subscribeAPI = new SubscribeAPI($this->payMe->getCashId(), $this->payMe->getCashKey());
        $this->sumToTins = app()->make(ConvertSumToTins::class);
    }

    /**
     * @throws RequestException
     * @throws PayMeException
     */
    public function handle(): void
    {
        $receipt = $this->createInvoiceForOrder();

        $receiptId = $receipt['_id'];
        $this->send($receiptId);

        PayMeReceipt::create([
            'order_id' => $this->order->id,
            'data' => $receipt
        ]);

        CheckPayMeReceiptStatus::dispatch($receiptId, $this->order)
            ->delay(now()->addSeconds(30));
    }

    /**
     * @throws RequestException
     * @throws PayMeException
     */
    public function createInvoiceForOrder() : array
    {
        $details = $this->makeGoodDetail($this->order->items);

        $response = $this->subscribeAPI->createReceipt(
            $this->sumToTins->convert($this->order->sum),
            ['order_id' => $this->order->id],
            $details
        );

        return $response['result']['receipt'];
    }

    /**
     * @throws PayMeException
     * @throws RequestException
     */
    public function send(string $receiptId): void
    {
        $this->subscribeAPI->sendReceipt($receiptId, $this->order->phone);
    }

    private function makeGoodDetail(iterable $items): array
    {
        $details = [];

        foreach ($items as $item) {
            $price = $this->sumToTins->convert($item->price);
            $product = $item->product;

            $details['items'][] = [
                "discount" => 0,
                "title" => $product->name,
                "price" => $price,
                "count" => $item->count,
                "code" => $product->id,
                "units" => 1,
                "vat_percent" => config('payment.vat_percent'),
                "package_code" => $product->id
            ];
        }

        return $details;
    }
}

<?php

namespace App\Services\Payment;

use App\Models\OnlinePharmacy\Entity\Order;
use App\Models\PayMeReceipt;
use App\Services\Delphi\ClickOrderService;
use App\Services\Delphi\PayMeOrderService;
use click\ClickException;
use click\Helper;
use click\Request;
use Error;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Throwable;

class ClickInvoiceService
{
    public ?Client $client = null;
    public ?Helper $helper = null;
    public array $request;
    private string $merchant_id = "";
    private string $service_id = "";
    private string $user_id = "";
    private string $secret_key = "";
    private ?Order $order = null;

    public function __construct()
    {
        $this->helper = new Helper();
        $this->request = (new Request())->post();
    }

    /**
     * @throws JsonException
     */
    public function handle(Order $order): static
    {
        $this->order = $order;
        $this->fillCredentials();
        $this->prepareClient();
        $this->sent2click();
        return $this;
    }

    private function fillCredentials(): void
    {

        $store = $this->order->store ?? null;
        if ($store) {
            $this->service_id = $store->Click_Service_id;
            $this->merchant_id = $store->Click_Merchant_id;
            $this->user_id = $store->Click_merchant_user_id;
            $this->secret_key = $store->Click_secret_key;
        } else {
            throw new Error('Store is not found!');
        }
    }

    private function prepareClient(): void
    {
        $this->client = new Client([
            "timeout" => 15,
            'base_uri' => 'https://api.click.uz/v2/merchant/',
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Auth' => (
                    $this->user_id . ':' . sha1($this->helper->timestamp . $this->secret_key) . ':' . $this->helper->timestamp
                )
            ]
        ]);

    }

    /**
     * @throws JsonException
     */
    private function sent2click(): void
    {
        $phone = $this->order->phone ?? "";
        $order_id = $this->order->id ?? 0;
        $amount = (float)($this->order->sum ?? 0);
        $res = $this->createInvoice($phone, $order_id, $amount);
        if (isset($res["error_code"]) && $res["error_code"] === 0) {
            $this->order->setStatus('wait_payment');
            PayMeReceipt::create([
                'order_id' => $this->order->id,
                'data' => $res
            ]);
            $this->log("Invoice successfully created! Invoice ID: " . $res["invoice_id"] . '.Order ID:' . $this->order->id);
        } else {
            $this->log("Error create invoice: " . $res["error_note"] . " OrderId:" . $order_id);
        }

    }

    public function createInvoice($phone, $order_id, $amount): array
    {
        $json = [
            'service_id' => $this->service_id,
            'merchant_trans_id' => "Order #" . $order_id,
            'phone_number' => $phone,
            'amount' => (float)$amount
        ];
        try {
            $response = $this->client->request('POST', 'invoice/create', [
                'json' => $json
            ]);
            if ($response->getStatusCode() === 200) {
                return (array)json_decode((string)$response->getBody(), 1, 512, JSON_THROW_ON_ERROR);
            }

            $this->log("Guzzle Status Error:" . $response->getReasonPhrase());
            return [
                "error_code" => '-9',
                "error_note" => $response->getReasonPhrase()
            ];
        } catch (GuzzleException $e) {
            $this->log("Guzzle Error:" . $e->getMessage());
            return [
                "error_code" => '-9',
                "error_note" => $e->getMessage()
            ];
        }
    }
    //////


    public function log(string $message): void
    {
        $file = storage_path("logs/click_log.log");
        $text = "Date: " . date("d.m.Y H:i:s") . PHP_EOL;
        $text .= "Url: " . request()?->url() . PHP_EOL;
        $text .= "POST: " . print_r($this->request, 1) . PHP_EOL;
        $text .= "Message: " . $message . PHP_EOL;
        $text .= "----------------------------" . PHP_EOL . PHP_EOL;
        file_put_contents($file, $text, FILE_APPEND);

    }

    /**
     * @throws ClickException
     * @throws JsonException
     */
    public function checkRequests($action): void
    {

        switch ($action) {
            case 'prepare':
                // getting the response of prepare method
                $this->response($this->prepare());
                break;
            case 'complete':
                // getting the response of complete method
                $this->response($this->complete());
                break;
            default:
                // return exception
                throw new ClickException('Incorrect request: action not found: ' . $action, ClickException::ERROR_METHOD_NOT_FOUND);
                break;
        }
    }

    /**
     * @throws JsonException
     */
    public function response($data): void
    {
        print_r(json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE));
        exit();
    }

    private function prepare(): array
    {
        // getting payment data from model
        $order_id = $this->orderID();
        if ($order_id) {
            $this->order = Order::findOrFail($order_id);
            $this->fillCredentials();
            // getting merchant_confirm_id and merchant_prepare_id
            $merchant_confirm_id = 0;
            $merchant_prepare_id = 0;
            if ($this->order) {
                $merchant_confirm_id = $this->order->id;
                $merchant_prepare_id = $this->order->id;
            }
            // check the request data to errors
            $result = $this->request_check($this->order);
            // complete the result to response
            $result += [
                'click_trans_id' => $this->request['click_trans_id'],
                'merchant_trans_id' => $this->request['merchant_trans_id'],
                'merchant_confirm_id' => $merchant_confirm_id,
                'merchant_prepare_id' => $merchant_prepare_id
            ];
            // change the payment status to waiting if request data will be possible
            return $result;
        }
        return [
            'error' => -5,
            'error_note' => 'Order does not exist'
        ];
    }

    private function orderID(): int
    {
        return (int)trim(str_replace("Order #", "", $this->request["merchant_trans_id"]));
    }

    private function request_check($order): array
    {
        // check to error in request from click
        $request = $this->request;
        if ($this->is_not_possible_data($request)) {
            return [
                'error' => -8,
                'error_note' => 'Error in request from click'
            ];
        }
        // prepare sign string as md5 digest
        $sign_string = md5(
            $request['click_trans_id'] .
            $request['service_id'] .
            $this->secret_key .
            $request['merchant_trans_id'] .
            ($request['action'] == 1 ? $request['merchant_prepare_id'] : '') .
            $request['amount'] .
            $request['action'] .
            $request['sign_time']
        );
        if ($sign_string != $request['sign_string']) {
            // return response array-like
            return [
                'error' => -1,
                'error_note' => 'SIGN CHECK FAILED!'
            ];
        }
        // check to action not found error
        if (!((int)$request['action'] == 0 || (int)$request['action'] == 1)) {
            // return response array-like
            return [
                'error' => -3,
                'error_note' => 'Action not found'
            ];
        }
        // get payment data by merchant_trans_id
        if (!$this->order) {
            // return response array-like
            return [
                'error' => -5,
                'error_note' => 'Order does not exist'
            ];
        }
        // check to already paid
        if ($this->order->status_id !== 5) {
            if ($this->order->status_id === 6) {
                // return response array-like
                return [
                    'error' => -9,
                    'error_note' => 'Transaction cancelled'
                ];
            }
            if ($this->order->status_id === 7) {
                // return response array-like
                return [
                    'error' => -4,
                    'error_note' => 'Already payed'
                ];
            }
            // return response array-like
            return [
                'error' => -4,
                'error_note' => 'Status is not wait payment'
            ];
        }
        // check to correct amount
        if (abs((float)$this->order->sum - (float)$request['amount']) > 0.01) {
            // return response array-like
            return [
                'error' => -2,
                "diff" => abs((float)$this->order->sum - (float)$request['amount']),
                'error_note' => 'Incorrect parameter amount'
            ];
        }
        // check status to transaction cancelled

        // return response array-like as success
        return [
            'error' => 0,
            'error_note' => 'Success'
        ];

    }

    private function is_not_possible_data($request): bool
    {
        if (!(
                isset($request['click_trans_id']) &&
                isset($request['service_id']) &&
                isset($request['merchant_trans_id']) &&
                isset($request['amount']) &&
                isset($request['action']) &&
                isset($request['error']) &&
                isset($request['error_note']) &&
                isset($request['sign_time']) &&
                isset($request['sign_string']) &&
                isset($request['click_paydoc_id'])
            ) || ($request['action'] == 1 && !isset($request['merchant_prepare_id']))) {
            return true;
        }
        return false;
    }

    private function complete(): array
    {

        // get the payment data from model
        $order_id = $this->orderID();
        if ($order_id) {
            $this->order = Order::findOrFail($order_id);
            $this->fillCredentials();
            $merchant_confirm_id = 0;
            $merchant_prepare_id = 0;
            if ($this->order) {
                $merchant_confirm_id = $this->order->id;
                $merchant_prepare_id = $this->order->id;
            }
            // check the request data to errors
            $result = $this->request_check($this->order);
            // prepare the data to response
            $result += [
                'click_trans_id' => $this->request['click_trans_id'],
                'merchant_trans_id' => $this->request['merchant_trans_id'],
                'merchant_confirm_id' => $merchant_confirm_id,
                'merchant_prepare_id' => $merchant_prepare_id
            ];
            if ($this->request['error'] < 0 && !in_array($result['error'], [
                    -4,
                    -9
                ], false)) {
                $this->order->setStatus('cancel');
                // update payment status to error if request data will be error
                $result = [
                    'error' => -9,
                    'error_note' => 'Transaction cancelled'
                ];

            } elseif ($result['error'] === 0) {
                // update payment status to confirmed if request data will be success
                ///send to delphi
                try {
                    $delphiOrderService = app()->make(ClickOrderService::class);
                    $order = $this->order->fresh();
                    $order->paid();
                    $delphiOrderService->create($order, $this->request['click_trans_id']);
                } catch (Throwable $e) {
                    $this->log("Delphi Send Error:".$e->getMessage());
                }

            }
            // return response array
            return $result;
        }
        return [
            'error' => -5,
            'error_note' => 'Order does not exist'
        ];

    }


    public function getPayLink(string $phone, int $id, $amount): string
    {
        $order_id = "Order #" . $id;
        return 'https://my.click.uz/services/pay?service_id=' . $this->service_id . '&merchant_id=' . $this->merchant_id . '&amount=' . (float)$amount . '&transaction_param=' . urlencode($order_id) . '&return_url=https://oxymed.uz/personal/';
    }
}

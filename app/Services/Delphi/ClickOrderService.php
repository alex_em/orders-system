<?php

namespace App\Services\Delphi;

use App\Models\Delphi\Order\Good;
use App\Models\Delphi\Order\Order as DelphiOrder;
use App\Models\OnlinePharmacy\Entity\Order;
use App\Models\OnlinePharmacy\Entity\Product;

class ClickOrderService
{
    public function create(Order $order, $payment_id = null)
    {
        /** @var DelphiOrder $delphiOrder */
        DelphiOrder::updateOrCreate(['Order_Id' => $order->id], [
            'Order_Id' => $order->id,
            'StoreId' => $order->store?->delphi_id,
            'Payment_type' => 'click',
            'Delivery_type' => 'online',
            'Is_payed' => $order?->status_id === 7 ? 1 : 0,
            'Summa' => $order?->sum,
            'Delivery_address' => $order?->address,
            'User_phone' => $order?->phone,
            'FIO' => $order?->fio,
            'note' => $order?->note,
            'is_self' => $order->is_self,
            'promo' => $order?->promo,
            'Click_payment_id' => $payment_id,
            'WebStatus' => $order?->status?->name
        ]);

        foreach ($this->mapGoods($order->items) as $mapGood) {
            $mapGood['Order_Id'] = $order->id;
            Good::updateOrCreate(["Order_Id" => $mapGood['Order_Id'], 'product_id' => $mapGood['product_id']], $mapGood);
        }
    }

    private function mapGoods(iterable $items): array
    {
        return $items->map(function ($item) {
            $product = Product::find($item->product_id);

            return [
                'product_id' => $product?->delphi_id,
                'Price' => $item->price,
                'Count' => $item->count,
            ];
        })
            ->toArray();
    }
}

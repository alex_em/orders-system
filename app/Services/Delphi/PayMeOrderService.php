<?php

namespace App\Services\Delphi;

use App\Models\Delphi\Order\Good;
use App\Models\Delphi\Order\Order as DelphiOrder;
use App\Models\OnlinePharmacy\Entity\Order;
use App\Models\OnlinePharmacy\Entity\Product;

class PayMeOrderService
{
    public function create(Order $order, $receiptId = null)
    {
        /** @var DelphiOrder $delphiOrder */
        DelphiOrder::updateOrCreate(['Order_Id' => $order->id],[
            'Order_Id' => $order->id,
            'StoreId' => $order->store?->delphi_id,
            'Payment_type' => 'payme',
            'Delivery_type' => 'online',
            'Is_payed' => true,
            'Summa' => $order?->sum,
            'Delivery_address' => $order?->address,
            'User_phone' => $order?->phone,
            'FIO' => $order?->fio,
            'note' => $order?->description,
            'is_self' => false,
            'promo' => $order?->promo,
            'ReceiptId' => $receiptId,
        ]);

        foreach ($this->mapGoods($order->items) as $mapGood) {
            $mapGood['Order_Id']=$order->id;
            Good::updateOrCreate(["Order_Id"=>$mapGood['Order_Id'], 'product_id'=>$mapGood['product_id']],$mapGood);
        }
    }

    private function mapGoods(iterable $items): array
    {
        return $items->map(function ($item) {
            $product = Product::find($item->product_id);

            return [
                'product_id' => $product?->delphi_id,
                'Price' => $item->price,
                'Count' => $item->count,
               ];
        })
            ->toArray();
    }
}

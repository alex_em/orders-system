<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Models\Delphi\Good;
use App\Models\Delphi\Pharmacy;
use App\Policies\ProductPolicy;
use App\Policies\PharmacyPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Good::class => ProductPolicy::class,
        Pharmacy::class => PharmacyPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        //
    }
}

<?php

namespace App\Policies;

class ProductPolicy
{
    public function create(): bool
    {
        return false;
    }

    public function delete(): bool
    {
        return false;
    }

    public function viewAny()
    {
        return true;
    }
}

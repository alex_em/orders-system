<?php

namespace App\Nova\Actions;

use App\Models\OnlinePharmacy\Entity\Order;
use App\Services\Payment\ClickInvoiceService;
use App\Services\Payment\PayMeInvoiceService;
use Error;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;
use Laravel\Nova\Http\Requests\NovaRequest;

class Invoice extends Action
{
    use InteractsWithQueue, Queueable;

    public $name = 'Выставить счет';


    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection $models
     * @return mixed
     * @throws BindingResolutionException
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $models->each(function (Order $order) {
            if($order->payment->code==="payme"){
                (new PayMeInvoiceService($order))->handle();
            }elseif($order->payment->code==="click"){
                (new ClickInvoiceService())->handle($order);
            }else{
                throw new Error('Order #'.$order->id.'`s payment system is unknown!');
            }

        });
    }

    /**
     * Get the fields available on the action.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [];
    }
}

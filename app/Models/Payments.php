<?php

namespace App\Models;

use App\Exceptions\PayMeException;
use App\Models\OnlinePharmacy\Entity\Store;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    use HasFactory;

    protected $casts = [
        'data' => 'array'
    ];

    public function store(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Store::class, 'pharmacy_id');
    }

    /**
     * @throws PayMeException
     */
    public function getPayMeCredentials(): PayMe
    {
        if (!isset($this->data['payments']['payme'])) {
            throw new PayMeException('Payme credentials not found');
        }
        return new PayMe(
            $this->data['payments']['payme']['cash_id'],
            $this->data['payments']['payme']['key'],
            $this->data['payments']['payme']['test_key'] ?? null
        );
    }
}

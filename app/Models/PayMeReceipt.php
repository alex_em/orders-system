<?php

namespace App\Models;

use App\Models\OnlinePharmacy\Entity\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayMeReceipt extends Model
{
    use HasFactory;

    protected $fillable = [
        'data',
        'order_id'
    ];

    protected $casts = [
        'data' => 'array'
    ];

    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}

<?php

namespace App\Models\OnlinePharmacy\Entity;

use App\Models\OnlinePharmacy\OrderDelivery;
use App\Models\OnlinePharmacy\OrderPayment;
use App\Models\OnlinePharmacy\OrderStatus;
use App\Models\OnlinePharmacy\User;
use App\Models\PayMeReceipt;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const CANCELED = 'cancel';
    const DONE = 'done';

    const PAID = 'payment';

    const IN_PROGRESS = 'doing';

    protected $connection = 'online_pharmacy';

    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function basket()
    {
        return $this->hasMany(Basket::class, 'order_id')->with("productInOrder");
    }

    public function items()
    {
        return $this->hasMany(Basket::class, 'order_id');
    }

    public function payment()
    {
        return $this->belongsTo(OrderPayment::class, 'payment_id');
    }

    public function delivery()
    {
        return $this->belongsTo(OrderDelivery::class, 'delivery_id');
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'status_id');
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function payMeReceipts(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PayMeReceipt::class, 'order_id');
    }

    public function setStatus(string $status): void
    {
        $status = OrderStatus::where('code', $status)->first();

        if ($status) {
            $this->status_id = $status->id;
            $this->save();
        }
    }

    public function inProgress()
    {
        //TODO dynamic statuses
        $this->status_id = 2;
        $this->save();
    }

    public function paid()
    {
        $this->status_id = 7;
        $this->save();
    }
}

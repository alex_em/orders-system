<?php

namespace App\Models\OnlinePharmacy\Entity;

use App\Models\OnlinePharmacy\Dict\City;
use App\Models\OnlinePharmacy\Dict\District;
use App\Models\OnlinePharmacy\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use MatanYadaev\EloquentSpatial\Objects\Point;
use Spatie\Translatable\HasTranslations;

class Store extends Model
{
    protected $connection = 'online_pharmacy';

    use HasTranslations;
    use HasFactory;

    public const CENTRAL_STORE = 1;
    protected $guarded = [];
    protected $casts = [
        'location' => Point::class
    ];
    public array $translatable = ['worktime', 'name', 'address'];

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function storage()
    {
        return $this->hasMany(Storage::class, 'store_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'store_id');
    }

    protected static function booted(): void
    {

        static::updated(static function ($store) {
            cache()->delete('stores_list');
            cache()->delete('central_store');
            self::userChange();


        });
        static::deleted(static function ($store) {
            cache()->delete('stores_list');
            cache()->delete('central_store');
            self::userChange();
        });
        static::saved(static function ($store) {
            cache()->delete('stores_list');
            cache()->delete('central_store');
            self::userChange();
        });
    }

    public static function userChange(): void
    {
        User::whereNotNull('store_id')
            ->whereNull('need_update_store')
            ->get(['id'])->each(function ($item) {
                $item->update(['need_update_store' => true]);
            });
    }
}

<?php

namespace App\Models\OnlinePharmacy\Entity;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{
    protected $connection = 'online_pharmacy';

    use HasFactory;
    use HasTranslations;
    use HasSlug;
    use Searchable;

    protected $casts = [
        'name' => 'array'
    ];
    public array $translatable = ['name', 'meta_desc', 'meta_title', 'meta_keywords', 'meta_h1', 'sale_name', 'doze', 'package', 'store_temp', 'expire'];
    public string $uniqueField = 'delphi_id';
    protected $guarded = [];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug')
            ->usingLanguage('ru');
    }

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function scopeWithPriceForStore(Builder $query, int $min = null, int $max = null)
    {
        $store_id = getCurrentStore();
        return $query
            ->addSelect([
                "products.*",
                "storages.available as available",
                DB::raw('IFNULL(storages.price * (100 - COALESCE(products.discount, 0)) / 100, 0) as price'),
                DB::raw('storages.price as old_price')
            ])
            ->leftJoin('storages', function ($join) use ($store_id) {
                $join->on('products.id', '=', 'storages.product_id')
                    ->where('storages.store_id', '=', $store_id);
            })
            ->when($min, function ($query) use ($min) {
                return $query->where('storages.price', '>=', $min);
            })
            ->when($max, function ($query) use ($max) {
                return $query->where('storages.price', '<=', $max);
            });
    }

    public function shouldBeSearchable()
    {
        return true;
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}


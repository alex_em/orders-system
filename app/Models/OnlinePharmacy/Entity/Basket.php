<?php

namespace App\Models\OnlinePharmacy\Entity;

use App\Models\OnlinePharmacy\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $connection = 'online_pharmacy';

    use HasFactory;

    protected $guarded = [];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id')
            ->select(["id", "name", "slug", "image", "producer_id", "receipt", "delphi_id"]);
    }

    public function productInOrder(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id')
            ->select(["id", "name", "slug", "image", "receipt", 'delphi_id']);
    }

}

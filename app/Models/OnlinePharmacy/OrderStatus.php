<?php

namespace App\Models\OnlinePharmacy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

class OrderStatus extends Model
{
    protected $connection = 'online_pharmacy';

    use HasFactory;
    use HasSlug;
    use HasTranslations;
    public array $translatable=["name", "desc"];
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('code')
            ->usingLanguage('ru');
    }

    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'code';
    }
}

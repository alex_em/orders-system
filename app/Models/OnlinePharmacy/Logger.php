<?php

namespace App\Models\OnlinePharmacy;

use App\Models\OnlinePharmacy\Entity\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Logger extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $connection = 'online_pharmacy';
    public static function addOrderLog($id, $text)
    {
        return self::create([
            'model' => Order::class,
            'page' => Str::limit(getCurPageParams(), 200),
            'object_id' => $id,
            'text' => "InOrders:" . $text,
            'user_id' => auth()->user()?->id,
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function array2string(array $changes): string
    {
        $res = [];
        foreach ($changes as $k => $v) {
            $res[] = $k . ":" . $v;
        }
        return implode(" | ", $res);
    }

}

<?php

namespace App\Models\OnlinePharmacy;

use App\Models\OnlinePharmacy\Entity\Basket;
use App\Models\OnlinePharmacy\Entity\Order;
use App\Models\OnlinePharmacy\Entity\Store;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use MatanYadaev\EloquentSpatial\Objects\Point;

class User extends Authenticatable
{
    protected $connection = 'online_pharmacy';

    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'date_of_birth' => 'date',
        'sms_code_expire' => 'datetime',
        'location' => Point::class
    ];

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }

    public function basket()
    {
        return $this->hasMany(Basket::class, 'user_id')->whereNull('order_id');
    }

    public function fullName()
    {
        return trim(implode(" ", [$this->name, $this->last_name, $this->middle_name]));
    }
}

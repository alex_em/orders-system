<?php

namespace App\Models\Delphi\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv';

    protected $table = 'WebPayments';

    public $timestamps = false;

    protected $guarded=[];
    /*protected $fillable = [
        'Order_Id',
        'StoreId',
        'Payment_type',
        'Delivery_type',
        'Is_payed',
        'Summa',
        'Delivery_address',
        'User_phone',
        'FIO',
        'note',
        'is_self',
        'promo',
        "ReceiptId",
    ];*/

    public function goods(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Good::class, 'Order_Id', 'Order_Id');
    }
}

<?php

namespace App\Models\Delphi\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv';

    protected $table = 'WebReserve';

    public $timestamps = false;
    protected $fillable = [
        'product_id',
        "Price",
        "Count",
        "Order_Id"
    ];
}

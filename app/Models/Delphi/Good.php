<?php

namespace App\Models\Delphi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv';

    protected $table = 'Good';

    protected $primaryKey = 'GoodId';


    public $timestamps = false;
}

<?php

namespace App\Models\Delphi;

use App\Models\Payments;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pharmacy extends Model
{
    use HasFactory;

    protected $connection = 'sqlsrv';

    protected $table = 'Branch';

    protected $primaryKey = 'BranchId';

    public $timestamps = false;
}

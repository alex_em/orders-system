<?php

namespace App\Models;

class PayMe
{
    public function __construct(private readonly string $cashId, private readonly string $cashKey, private readonly ?string $cashTestKey)
    {
    }

    public function getCashId(): string
    {
        return $this->cashId;
    }

    public function getCashKey(): string
    {
        return $this->cashKey;
    }

    public function getCashTestKey(): ?string
    {
        return $this->cashTestKey;
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductResource;
use App\Models\OnlinePharmacy\Entity\Product;
use Illuminate\Database\Eloquent\Builder;
use Orion\Http\Controllers\Controller;
use Orion\Http\Requests\Request;

class ApiGoodsController
{
    public function index(Request $request)
    {
        $query = Product::query();

        $search = $request->get('search');

        if ($queryParam = data_get($search, 'value')) {
            $query->where('name->ru', 'LIKE',"%$queryParam%");
        }


        return ProductResource::collection($query->limit(20)->get());
    }

    public function show(int $id)
    {
        return new ProductResource(Product::findOrFail($id));
    }
}

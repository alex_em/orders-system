<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Services\OnlinePharmacy\OrderService;
use Illuminate\Http\Request;

class OnlinePharmacyOrderApiController extends Controller
{
    public function __construct(private readonly OrderService $orderService)
    {
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $this->orderService->create($request->all());

        return response()->json(['success' => true]);
    }
    public function update(int $orderId, Request $request): \Illuminate\Http\JsonResponse
    {
        $this->orderService->update($orderId, $request->all());

        return response()->json(['success' => true]);
    }
}

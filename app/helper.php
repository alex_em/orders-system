<?php

use App\Models\OnlinePharmacy\Entity\Store;
use App\Models\OnlinePharmacy\Data\OnlinePharmacy\OnlinePharmacy\StaticBlock;
use App\Models\OnlinePharmacy\Data\OnlinePharmacy\OnlinePharmacy\User;
use App\Zak\Image;
use App\Zak\Search;
use App\Zak\zCache;
use Illuminate\Database\Schema\Blueprint;

function get_static_block($code, $default = '')
{
    return StaticBlock::byCode($code, $default);
}

function btf($n): string
{
    return number_format($n, false, '', ' ');
}

function forms(int $n, $forms = array("товар", "товара", "товаров"))
{
    return $n % 10 === 1 && $n % 100 !== 11 ? $forms[0] : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $forms[1] : $forms[2]);
}

function getCurPageParams($add = [], $remove = []): string
{
    $r = request()?->all();
    if ($add) {
        $r = array_merge($r, $add);
    }
    if ($remove) {
        Arr::forget($r, $remove);
        return trim(request()?->url() . '?' . http_build_query($r), "?");
    }
    return '/' . trim(request()?->path() . "?" . http_build_query($r), "?");
}

function getCurPage(): ?string
{
    return request()?->url();
}

function isIndex(): bool
{
    return request()?->localizedRouteIs('home');
}

function inDir($d): bool
{
    $path = request()?->path();
    if (!str_starts_with($path, '/')) {
        $path = '/' . $path;
    }
    return str_starts_with($path, $d);
}

function getImage($p): string
{
    return $p ? asset('/storage/' . $p) : Image::DEFAULT_IMAGE;
}

function resize($file, $width, $height, $crop = false): string
{
    return Image::init()->Resize($file, $width, $height, $crop);
}


function setMeta(Blueprint $table): void
{
    $table->json('meta_desc')->nullable();
    $table->json('meta_title')->nullable();
    $table->json('meta_keywords')->nullable();
    $table->json('meta_h1')->nullable();
}

function setMetaDown(Blueprint $table): void
{
    $table->dropColumn('meta_desc');
    $table->dropColumn('meta_title');
    $table->dropColumn('meta_keywords');
    $table->dropColumn('meta_h1');
}

function discount_price(float $price, float $discount): string
{
    $percent = 100 - $discount;
    return btf($price * $percent / 100);
}

function clear_spaces($text): string
{
    return str_replace(" ", "", $text);
}

function isPhone(string $phone): bool|string
{

    $phone = preg_replace('/[^0-9+]/', '', $phone);
    $test = preg_match('/\+?\d{9,}/', $phone, $matches);
    if ($test) {
        return "+998" . substr($phone, -9);
    }

    return false;
}

function getUserLocation($onlyCookie = false)
{
    static $value;
    if ($value) {
        return $value;
    }
    if (!$onlyCookie && ($user = auth()->user())) {

        /**@var User $user */
        return $user->location ? $value = array_filter([
            'location' => implode(",", array_filter([$user->location?->latitude, $user->location?->longitude])),
            'addr' => $user->address,
            'store_id' => $user->store_id
        ]) : [];
    }
    try {
        return $value = array_filter(json_decode(
            request()?->cookie("user-location") ?? "",
            1,
            512,
            JSON_THROW_ON_ERROR
        ));
    } catch (Throwable $e) {

    }
    return $value = [];
}

function getNearestStore(array $loc): Store|null
{
    if (isset($loc[0], $loc[1])) {
        $srid = config('services.map.srid');
        return Store::select(
            '*',
            \DB::raw("ST_Distance_Sphere(location, ST_GeomFromText('POINT($loc[0] $loc[1])', $srid))/1000 as distance")
        )
            ->whereActive(true)
            ->whereNotNull('location')
            ->orderBy('distance')
            ->first();
    }
    return null;

}

function getCentralStore()
{
    return zCache::rememberForever("central_store", function () {
        return Store::whereMain(true)->first()?->id;
    });
}

function getCurrentStore()
{
    /*
    * @TODO zak handle

    */
    return getCentralStore();
    $userLocation = getUserLocation();
    return $userLocation['store_id'] ?? getCentralStore();
}

function searchable($a)
{
    $a = doSearchable($a);
    $result = [];
    doOneArray($a, $result);
    return $result;
}

function doOneArray($a, &$result)
{
    if (is_array($a)) {
        foreach ($a as $v) {
            doOneArray($v, $result);
        }
    } else {
        $result[] = $a;
    }
}

function doSearchable($a)
{
    if (is_array($a)) {
        foreach (array_filter($a) as $k => $v) {

            $a[$k] = doSearchable($v);
        }
    } elseif ($a) {
        $a = (new Search($a))->getWords();
    }
    return $a;
}

function seo($is404 = false): string
{
    return \App\Zak\Seo::show($is404);
}

?>

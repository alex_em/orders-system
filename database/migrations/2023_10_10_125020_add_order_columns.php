<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->json('data')->after('items')->nullable();
        });

        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn(['city', 'district']);
            $table->string('address')->after('online_pharmacy_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['data']);
        });

        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn(['address']);
            $table->string('city')->nullable();
            $table->string('district')->nullable();
        });
    }
};
